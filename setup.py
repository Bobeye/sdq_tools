from setuptools import setup
import sys
# from pip.req import parse_requirements

if sys.version.split(".")[0] == "3":
    requirements = ["numpy==1.19.5",
                    "scipy==1.5.4", "scikit-learn==0.24.2"]
elif sys.version.split(".")[0] == "2":
    requirements= ["numpy==1.13.3",
                    "scipy==1.2.3", "scikit-learn==0.20.4"]
else:
    raise ValueError

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='sdq_tools',
   version='1.0',
   description='Python Library for Safe Domain Quantification',
   license="MIT",
   long_description=long_description,
   author='Bowen Weng',
   author_email='bowen.weng.1991@gmail.com',
   url="",
   packages=['sdq_tools','sdq_tools.utils'],
   install_requires = requirements
)
