import numpy as np 
import random
from sklearn.neighbors import KDTree
from itertools import product
from collections import deque
import os
import pickle

from .utils.graph import Graph
from .operational_state_space import OperationalStateSpace


class DeltaCoveringDomain:

	def __init__(self, OSS, delta, D_s=None, D_u=None):
		self.OSS = OSS
		self.DIM = self.OSS.DIM
		if type(delta) != list:
			raise ValueError("delta must be in list type")
		if len(delta) == 1:
			self.delta = np.ones(self.DIM)*delta[0]
		else:
			self.delta = np.array(delta)
		lists = []
		for k in range(self.DIM):
			lists += [np.array([-self.delta[k],0,self.delta[k]])]
		self.offsets = np.array(list(product(*lists)))

		self.D_u_graph = Graph()
		self.D_s_graph = Graph()

		if D_u is None:
			self.D_u_tree = None
		else:
			self.D_u_tree = KDTree(D_u)
			for p in D_u:
				self.D_u_graph.addVertex(tuple(p.tolist()))

		if D_s is None:
			points = self.OSS.build_delta_covering_set(self.delta)
			self.D_s_tree = KDTree(points)        
			for p in points:
				self.D_s_graph.addVertex(tuple(p.tolist()))    
		else:
			self.D_s_tree = KDTree(D_s)
			for p in D_s:
				self.D_s_graph.addVertex(tuple(p.tolist()))
		
	@property
	def D_s(self):
		return self.D_s_graph.vertices()

	@property
	def D_u(self):
		return self.D_u_graph.vertices()

	def update_D_s_tree(self):
		self.D_s_tree = KDTree(np.array(list(self.D_s())))

	def update_D_u_tree(self):
		if len(self.D_u())>0:
			self.D_u_tree = KDTree(np.array(list(self.D_u())))
		else:
			self.D_u_tree = None

	def add_point(self, p):
		if type(p) != list:
			p = p.tolist()
		self.D_s_graph.addVertex(tuple(p))
		self.update_D_s_tree()
	
	def remove_point(self, p):
		if type(p) != list:
			p = p.tolist()
		self.D_u_graph.addVertex(tuple(p))
		self.update_D_u_tree()
		reachable2p = self.D_s_graph.reachableVertercies(tuple(p))
		if reachable2p != []:
			for rp in reachable2p:
				self.D_s_graph.removeVertex(rp)
				self.D_u_graph.addVertex(rp)
				self.update_D_u_tree()
		self.update_D_s_tree()

	def add_edge(self, e):
		(ps, pe) = e
		if type(ps) != list:
			ps = ps.tolist() 
		if type(pe) != list:
			pe = pe.tolist()
		ps_in_Ds = self.is_point_in_Ds(ps)
		if not ps_in_Ds:
			self.add_point(ps)
		pe_in_Ds = self.is_point_in_Ds(pe)
		if not pe_in_Ds:
			self.add_point(pe)
		if (not ps_in_Ds) and (not pe_in_Ds):
			self.D_s_graph.addEdge((tuple(ps), tuple(pe)))

	def get_neigh_in_Ds(self, p):
		if type(p) != list:
			p = p.tolist()
		p = np.array(p)
		vec = np.all(abs(p-np.array(list(self.D_s())))<=self.delta, axis=1)
		if np.any(vec):
			idx = np.where(vec)[0]
			return np.array(list(self.D_s()))[idx]
		else:
			return None

	def is_neigh_in_Ds(self, p):
		if type(p) != list:
			p = p.tolist()
		p = np.array(p)
		ref_ps = p + self.offsets
		for ref_p in ref_ps:
			ref_in_n = np.any(np.all(abs(ref_p-np.array(list(self.D_s())))<=self.delta,axis=1))
			if not ref_in_n:
				return False 
		return True


	def is_neigh_in_Du(self, p):
		if len(self.D_u())>0:
			if type(p) != list:
				p = p.tolist()
			p = np.array(p)
			ref_ps = p + self.offsets
			for ref_p in ref_ps:
				ref_in_n = np.any(np.all(abs(ref_p-np.array(list(self.D_u())))<=self.delta,axis=1))
				if not ref_in_n:
					return False 
			return True
			return False
		else:
			return False

	def is_point_in_Ds(self, p):
		if type(p) != list:
			p = p.tolist()
		p = np.array(p)
		return np.any(np.all(abs(p-np.array(list(self.D_s())))<=self.delta, axis=1),axis=0)

	def is_point_in_Du(self, p):
		if len(self.D_u())==0:
			return False 
		else:
			if type(p) != list:
				p = p.tolist()
			p = np.array(p)
			return np.any(np.all(abs(p-np.array(list(self.D_u())))<=0, axis=1),axis=0)

	def sample(self, maxIter=10000):
		found = False 
		while not found:
			p = self.OSS.sample()
			if self.is_point_in_Ds(p):
				cp = p
				found = True

		return cp.tolist()         

			
class DeltaCoveringSDQ:

	def __init__(self, OSS, delta, Ds0=None, Du0=None, epsilon=0.1, beta=0.01, path=None):
		self.total_trajs, self.total_fails = 0, 0
		if path is None:
			self.OSS = OSS
			self.epsilon = epsilon 
			self.beta = beta
			self.removal_candidates_max = 10
			self.N = 0
			self.N_max = np.log(self.beta)/np.log(1-self.epsilon)
			print ("Desired safe transitions: %d" % self.N_max)

			self.domain = DeltaCoveringDomain(OSS, delta, D_s=Ds0, D_u=Du0)
			self.delta = np.array(delta)
			self.removal_candidates = set()
		else:
			with open(path+"OSS.pkl", "rb") as p:
				OSS_config = pickle.load(p)
			self.OSS = OperationalStateSpace(OSS_config["lower_bounds"], OSS_config["upper_bounds"])
			self.OSS.load(path+"OSS.pkl")
			with open(path+"ODD.pkl", "rb") as p:
				ODD_config = pickle.load(p)
			self.epsilon = ODD_config["epsilon"]
			self.beta = ODD_config["beta"]
			self.N = 0 
			self.N_max = np.log(self.beta)/np.log(1-self.epsilon)
			print ("Desired safe transitions: %d" % self.N_max)

			Ds0 = ODD_config["D_s"]
			self.domain = DeltaCoveringDomain(self.OSS, ODD_config["delta"].tolist(), D_s=Ds0, D_u=None)
			self.delta = ODD_config["delta"] if delta==[] else np.array(delta)
			self.removal_candidates = set()

	def add_to_removal_candidates(self, p):
		if len(self.removal_candidates)<self.removal_candidates_max:
			self.removal_candidates.add(p)

	def fit_traj(self, traj, failure=False):
		self.total_trajs += 1 
		if failure:
			self.total_fails += 1
		traj = np.clip(np.array(traj), self.OSS.lower_bounds, self.OSS.upper_bounds)
		terminate = False
		if failure:
			prev_s = None
			for s in traj:
				self.domain.remove_point(s)
				if prev_s is None or np.any(abs(s-prev_s)>self.domain.delta):
					self.add_to_removal_candidates(tuple(s.tolist()))
					# self.removal_candidates.add(tuple(s.tolist()))
					prev_s = s
			self.N = 0
		else:
			i = 0
			inside_domain = True
			while i < traj.shape[0]-1:
				#print (i, traj.shape[0]-1, end="\r")
				D_s_size, D_u_size = len(self.domain.D_s()), len(self.domain.D_u())
				ps, pe = traj[i].tolist(), traj[i+1].tolist()				
				if self.domain.is_point_in_Du(ps) or self.domain.is_point_in_Du(pe): 
					prev_s = None
					for s in traj[0:i+1]:
						self.domain.remove_point(s)
						if prev_s is None or np.any(abs(s-prev_s)>self.domain.delta):
							self.add_to_removal_candidates(tuple(s.tolist()))
							# self.removal_candidates.add(tuple(s.tolist()))
							prev_s = s
					self.N = 0
					break
				else:
					if not self.domain.is_point_in_Ds(ps):
						self.add_to_removal_candidates(tuple(ps))
						# self.removal_candidates.add(tuple(ps))
					if not self.domain.is_point_in_Ds(pe):
						self.add_to_removal_candidates(tuple(pe))
						# self.removal_candidates.add(tuple(pe))
					self.domain.add_edge((ps, pe))
					if (D_u_size != len(self.domain.D_u())) and inside_domain:
						inside_domain = False
					if (D_s_size != len(self.domain.D_s()))  and inside_domain:
						inside_domain = False
				i += 1
			if inside_domain and len(self.removal_candidates)==0:
				self.N += 1
			else:
				self.N = 0
			if self.N >= self.N_max:
				terminate = True
			
		return terminate
	 
	def get_s0(self, dist=None):
		if len(self.removal_candidates)>0:
			tp = self.removal_candidates.pop()
			p = np.array(list(tp))
			_, ind = self.domain.D_s_tree.query(p.reshape(1,-1), k=1)
			cp = self.domain.D_s_tree.get_arrays()[0][ind[0][0]]
			return cp 
		arr = self.domain.D_s_tree.get_arrays()[0]
		ind = np.random.choice(arr.shape[0],size=1)[0]
		return arr[ind]

	def save(self, path):
		if not os.path.exists(path):
			os.makedirs(path)
		self.OSS.save(path+"OSS.pkl")
		ODD_config = dict() 
		ODD_config["epsilon"] = self.epsilon
		ODD_config["beta"] = self.beta
		ODD_config["delta"] = self.domain.delta 
		ODD_config["D_s"] = np.array(list(self.domain.D_s())) 
		ODD_config["D_u"] = np.array(list(self.domain.D_u()))
		ODD_config["trajs"] = self.total_trajs 
		ODD_config["fails"] = self.total_fails
		with open(path+"ODD.pkl", "wb") as p:
			pickle.dump(ODD_config, p)
