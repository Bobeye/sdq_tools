import numpy as np  
from itertools import product
import pickle

class OperationalStateSpace:

	def __init__(self, lower_bounds, upper_bounds, DIM=None, seed=None):
		self.lower_bounds = np.array(lower_bounds)
		self.upper_bounds = np.array(upper_bounds)
		self.DIM = DIM if DIM is not None else self.lower_bounds.shape[0]
		self.Ks = np.vstack([np.eye(self.DIM), -np.eye(self.DIM)])
		self.bs = np.hstack([self.upper_bounds, self.lower_bounds])
		self.seed = seed 
		np.random.seed(self.seed)

	def save(self, path):
		with open(path, "wb") as p:
			pickle.dump(self.__dict__, p)

	def load(self, path):
		with open(path, "rb") as p:
			self.__dict__ = pickle.load(p)

	def is_point_in_OSS(self, p):
		p = np.array(p)
		return np.all(np.dot(self.Ks, p)<=self.bs)

	def sample(self):
		inbound = False
		while not inbound:
			p = np.random.uniform(self.lower_bounds, self.upper_bounds)
			if self.is_point_in_OSS(p):
				inbound = True 
		return p
	
	def build_delta_covering_set(self, delta, relax=1):
		relax = np.clip(relax, 1, 2)
		lists = []
		for k in range(self.DIM):
			lists += [np.linspace(self.lower_bounds[k], self.upper_bounds[k], num=int((self.upper_bounds[k]-self.lower_bounds[k])/(delta[k]*relax))+1, endpoint=True)]
		return np.array(list(product(*lists)))
