from collections import defaultdict, deque

class Graph:
	"""
	A Graph Class in Python
	"""
	def __init__(self,gdict=None):
		if gdict is None:
			gdict = {}
		self.gdict = gdict

	def edges(self):
		edgename = []
		for vrtx in self.gdict:
			for nxtvrtx in self.gdict[vrtx]:
				if {nxtvrtx, vrtx} not in edgename:
					edgename.append({vrtx, nxtvrtx})
		return edgename

	def vertices(self):
		return self.gdict.keys

	def addVertecies(self, vertices):
		for vrtx in vertices:
			self.addVertex(vrtx)

	def addVertex(self, vrtx):
		if vrtx not in self.gdict:
			self.gdict[vrtx] = []

	def removeVertex(self, vrtx):
		if vrtx in self.gdict:
			del self.gdict[vrtx]

	def addEdges(self, edges):
		for edge in edges:
			self.addEdge(edge)

	def addEdge(self, edge):
		edge = set(edge)
		(vrtx1, vrtx2) = tuple(edge)
		if vrtx1 in self.gdict:
			self.gdict[vrtx1].append(vrtx2)
		else:
			self.gdict[vrtx1] = [vrtx2]

	def removeEdge(self, edge):
		edge = set(edge)
		(vrtx1, vrtx2) = tuple(edge)
		if vrtx1 in self.gdict:
			self.gdict[vrtx1].discard(vrtx2)
			if len(self.gdict[vrtx1])==0:
				del self.gdict[vrtx1]

	def adjVertecies(self, vrtx):
		adj = set()
		for v in self.gdict:
			if vrtx in self.gdict[v]:
				adj.add(v)

		return adj

	def reachableVertercies(self, vrtx):
		queue = deque()
		queue.append(vrtx)
		visited = dict()
		for key in self.gdict.keys():
			visited[key] = 0
		reachable = []
		while (len(queue) > 0):
			v = queue.popleft()
			reachable.append(v)
			for nb in self.adjVertecies(v):
				if visited[nb] == 0:
					visited[nb] = 1
					queue.append(nb)

		return reachable

	def looseEnds(self):
		loose_ends = []
		for edge in self.edges:
			if edge[1] not in self.gdict.keys():
				loose_ends += [edge[1]]
		return loose_ends

# if __name__ == "__main__":
# 	import numpy as np 
# 	g = Graph()
# 	a = np.array([1.2222345,5.444450987])
# 	g.addVertex(tuple(a.tolist()))
# 	print (g.gdict)