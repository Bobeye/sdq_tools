# SDQ_Tools

Safe domain quantification algorithm with <img src="https://latex.codecogs.com/svg.image?\bg_white&space;\epsilon\delta" title="\bg_white \epsilon\delta" />-almost safe set based methods.

## Building & installing

Run `python setup.py build` to build, and `python setup.py install` to install.
Alternatively, if you want an in-place build that puts the compiled library right in
the current directory, run `python setup.py build_ext --inplace`. Run `python setup.py clean --all` to clean up the build

## Example

Refer to our [Openpilot-in-Carla](https://github.com/pgchui/openpilot_in_carla) project for an example on evaluating the safety performance of [Openpilot](https://github.com/commaai/openpilot) integrating with [Carla](https://carla.org/) in the lead-obstacle and lead-following testing regime. 

## Dependencies
The code-base is develped with minor dependency requirements. The code has been tested on Ubuntu 16.04 and Ubuntu 18.04 machines with Python 3.6.9, Numpy 1.19.5, Scipy 1.5.4 and Scikit-learn 0.24.2. The configuration should generalize to other envionemts as well with some very minor changes in the `setup.py`.


Citing
------
If you find this code useful in your work, please consider citing our paper: [Weng et al.,T-IV, Preprint](https://arxiv.org/abs/2104.09595) and [Weng et al., RA-L, Preprint](https://arxiv.org/abs/2110.02331):

```
@article{weng2021towards,
  title={Towards guaranteed safety assurance of automated driving systems with scenario sampling: An invariant set perspective},
  author={Weng, Bowen and Ruiz, Linda Jenny Capito and Ozguner, Umit and Redmill, Keith},
  journal={IEEE Transactions on Intelligent Vehicles},
  year={2021},
  publisher={IEEE}
}
```
```
@article{weng2021formal,
  title={A Formal Characterization of Black-Box System Safety Performance with Scenario Sampling},
  author={Weng, Bowen and Capito, Linda and Ozguner, Umit and Redmill, Keith},
  journal={IEEE Robotics and Automation Letters},
  volume={7},
  number={1},
  pages={199--206},
  year={2021},
  publisher={IEEE}
}
```
