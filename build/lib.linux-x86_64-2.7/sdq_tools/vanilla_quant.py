import numpy as np 

from .utils.graph import Graph
from .operational_state_space import OperationalStateSpace

class VanillaDomain:
    
    D_s_graph = Graph()
    D_u_graph = Graph()

    def __init__(self, OSS) -> None:
        self.OSS = OSS

    @property
    def D_s(self):
        return self.D_s_graph.vertices()

    @property
    def D_u(self):
        return self.D_u_graph.vertices()

    def add_point(self, p):
        if type(p) != list:
            p = p.tolist()
        self.D_s_graph.addVertex(tuple(p))
    
    def remove_point(self, p):
        if type(p) != list:
            p = p.tolist()
        reachable2p = self.D_s_graph.reachableVertercies(tuple(p))
        if reachable2p != []:
            for rp in reachable2p:
                self.D_s_graph.removeVertex(rp)
                self.D_u_graph.addVertex(rp)

    def add_edge(self, e):
        (ps, pe) = e
        if type(ps) != list:
            ps = ps.tolist() 
        if type(pe) != list:
            pe = pe.tolist()
        self.D_s_graph.addEdge((tuple(ps), tuple(pe)))

    def is_point_in_Ds(self, p):
        if type(p) != list:
            p = p.tolist()
        return tuple(p) in self.D_s()

    def is_point_in_Du(self, p):
        if type(p) != list:
            p = p.tolist()
        return tuple(p) in self.D_u()

    def sample(self):
        inbound = False 
        while not inbound:
            p = self.OSS.sample()
            if not self.is_point_in_Du(p):
                inbound = True
        return p 

class VanillaSDQ:

    def __init__(self, OSS, epsilon=0.1, beta=0.01):
        self.epsilon = epsilon 
        self.beta = beta
        self.N = 0
        self.N_max = np.log(self.beta)/np.log(1-self.epsilon)
        print ("Desired safe transitions: %d" % self.N_max)

        self.domain = VanillaDomain(OSS)

    def fit_traj(self, traj, failure=False):
        traj = np.array(traj)
        terminate = False
        if failure:
            for s in traj:
                self.domain.remove_point(s)
                self.N = 0
        else:
            for i in range(traj.shape[0]-1):
                ps, pe = traj[i].tolist(), traj[i+1].tolist()
                if self.domain.is_point_in_Du(pe):
                    for j in range(i):
                        self.domain.remove_point(traj[j].tolist())
                    self.N = 0
                    break
                else:
                    self.domain.add_edge((ps, pe))
                    self.N += 1
                if self.N >= self.N_max:
                    terminate = True
                    break 

        return terminate
        
    def get_s0(self):
        s0 = self.domain.sample()

        return s0

        
